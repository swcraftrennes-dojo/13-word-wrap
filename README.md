# Word wrap

Write a function `WordWrap(content string, lineLength uint)` accepting an arbitrary string (_content_) and a positive int (_line length_), which returns an array of strings (_lines_) so that:

1.  the initial content equals the concatenation of the resulting lines (adding some space/newline characters in-between if necessary);
1.  each resulting string has at most line length characters;
1.  resulting strings do not start or end with space characters;
1.  lines would overflow if extended with the first word on the next line;
1.  words from the initial content are not split over multiple lines, **unless necessary** (i.e., word-breaking is allowed if and only if the word is longer than the requested line length, and no other word is found on the line);
1.  punctuation (`.,:;!?-`) should always be attached to the word before it;
1.  line breaks in the original content are preserved (without the newline character).

For example:

```go
WordWrap("Lorem ipsum dolor sit amet.", 80)
    => ["Lorem ipsum dolor sit amet."]
WordWrap("Lorem ipsum dolor sit amet.", 10)
    => ["Lorem", "ipsum", "dolor sit", "amet."]
WordWrap("Lorem ipsum dolor sit amet.", 3)
    => ["Lor", "em", "ips", "um", "dol", "or", "sit", "ame", "t."]
WordWrap("Lorem ipsum dolor\n\nsit amet.", 10)
    => ["Lorem", "ipsum", "dolor", "", "sit amet."]
WordWrap(`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed mi id purus pellentesque sodales ut sed libero. Suspendisse justo eros, feugiat at ultricies vitae, tempus non est. Praesent dolor dolor, gravida vel neque ut, vulputate tincidunt massa. Curabitur non metus nisi. Morbi pretium pulvinar diam sodales convallis. In hac habitasse platea dictumst. Nunc interdum lectus eu dignissim euismod. Etiam congue at massa in scelerisque. Proin ornare sit amet metus at rhoncus. Nunc justo quam, aliquam id ipsum a, feugiat tincidunt erat.

Quisque ligula lectus, auctor quis commodo sit amet, varius nec massa. Vestibulum molestie, sem sed eleifend gravida, enim lectus posuere odio, ut pellentesque ligula purus a nisi. Curabitur aliquam eros vitae lacus scelerisque, sit amet dapibus mi pulvinar. Duis non mauris nunc. Nunc eget libero odio. Donec sed ipsum sed magna lacinia varius. In commodo nibh non risus elementum, et accumsan risus fringilla. Fusce nec fringilla arcu, a tincidunt est. Sed vulputate nibh vitae porttitor sodales. Quisque odio purus, fringilla quis mi at, varius volutpat libero. Vestibulum dictum mauris ut diam volutpat, et imperdiet orci congue. Nullam commodo condimentum viverra. Morbi dictum est sed turpis gravida lacinia. In hac habitasse platea dictumst. Quisque nec egestas mi, quis rutrum tortor.`, 80)
    => [
           "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed mi id purus",
           "pellentesque sodales ut sed libero. Suspendisse justo eros, feugiat at ultricies",
           "vitae, tempus non est. Praesent dolor dolor, gravida vel neque ut, vulputate",
           "tincidunt massa. Curabitur non metus nisi. Morbi pretium pulvinar diam sodales",
           "convallis. In hac habitasse platea dictumst. Nunc interdum lectus eu dignissim",
           "euismod. Etiam congue at massa in scelerisque. Proin ornare sit amet metus at",
           "rhoncus. Nunc justo quam, aliquam id ipsum a, feugiat tincidunt erat.",
           "",
           "Quisque ligula lectus, auctor quis commodo sit amet, varius nec massa.",
           "Vestibulum molestie, sem sed eleifend gravida, enim lectus posuere odio, ut",
           "pellentesque ligula purus a nisi. Curabitur aliquam eros vitae lacus",
           "scelerisque, sit amet dapibus mi pulvinar. Duis non mauris nunc. Nunc eget",
           "libero odio. Donec sed ipsum sed magna lacinia varius. In commodo nibh non",
           "risus elementum, et accumsan risus fringilla. Fusce nec fringilla arcu, a",
           "tincidunt est. Sed vulputate nibh vitae porttitor sodales. Quisque odio purus,",
           "fringilla quis mi at, varius volutpat libero. Vestibulum dictum mauris ut diam",
           "volutpat, et imperdiet orci congue. Nullam commodo condimentum viverra. Morbi",
           "dictum est sed turpis gravida lacinia. In hac habitasse platea dictumst. Quisque",
           "nec egestas mi, quis rutrum tortor.",
       ]
```
